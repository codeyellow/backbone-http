const rollup = require('rollup');
const babel = require('rollup-plugin-babel');

rollup.rollup({
    entry: './index.js',
    external: [
        'jquery',
    ],
    plugins: [
        babel({
            exclude: 'node_modules/**',
        }),
    ],
}).then((bundle) => {
    bundle.write({
        format: 'umd',
        globals: {
            jquery: 'jQuery',
        },
        moduleId: 'backbone-http',
        moduleName: 'backboneHttp',
        dest: 'dist/backbone-http.js',
    });
}).catch((err) => {
    console.log(String(err));
    process.exit(1);
});
