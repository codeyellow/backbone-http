import $ from 'jquery';

let failHandler = function () {};

function isObject(obj) {
    const type = typeof obj;
    return type === 'function' || type === 'object' && !!obj;
}

function request(method, url, data, options) {
    options || (options = {});

    let ajaxOptions = {
        dataType: 'json',
        contentType: 'application/json',
        method,
        url,
        data: isObject(data) && method !== 'get' && !options.rawData ? JSON.stringify(data) : data,
    };

    if (options.ajaxOptions) {
        ajaxOptions = Object.assign(ajaxOptions, options.ajaxOptions);
    }

    const xhr = $.ajax(ajaxOptions);

    if (options.destroyWith) {
        options.destroyWith.once('destroy', () => {
            xhr.abort();
        });
    }

    if (!options.skipFailHandler) {
        xhr.fail(failHandler);
    }

    return xhr;
}

export default {
    get: (...args) => request.apply(this, ['get', ...args]),
    post: (...args) => request.apply(this, ['post', ...args]),
    patch: (...args) => request.apply(this, ['patch', ...args]),
    put: (...args) => request.apply(this, ['put', ...args]),
    delete: (...args) => request.apply(this, ['delete', ...args]),
    setFailHandler(handler) {
        failHandler = handler;
    },
};
