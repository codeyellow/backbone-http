(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('jquery')) :
  typeof define === 'function' && define.amd ? define('backbone-http', ['jquery'], factory) :
  (global.backboneHttp = factory(global.jQuery));
}(this, function ($) { 'use strict';

  $ = 'default' in $ ? $['default'] : $;

  var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
  };

  var _this = undefined;

  var failHandler = function failHandler() {};

  function isObject(obj) {
      var type = typeof obj === 'undefined' ? 'undefined' : _typeof(obj);
      return type === 'function' || type === 'object' && !!obj;
  }

  function request(method, url, data, options) {
      options || (options = {});

      var ajaxOptions = {
          dataType: 'json',
          contentType: 'application/json',
          method: method,
          url: url,
          data: isObject(data) && method !== 'get' && !options.rawData ? JSON.stringify(data) : data
      };

      if (options.ajaxOptions) {
          ajaxOptions = Object.assign(ajaxOptions, options.ajaxOptions);
      }

      var xhr = $.ajax(ajaxOptions);

      if (options.destroyWith) {
          options.destroyWith.once('destroy', function () {
              xhr.abort();
          });
      }

      if (!options.skipFailHandler) {
          xhr.fail(failHandler);
      }

      return xhr;
  }

  var index = {
      get: function get() {
          for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
              args[_key] = arguments[_key];
          }

          return request.apply(_this, ['get'].concat(args));
      },
      post: function post() {
          for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
              args[_key2] = arguments[_key2];
          }

          return request.apply(_this, ['post'].concat(args));
      },
      patch: function patch() {
          for (var _len3 = arguments.length, args = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
              args[_key3] = arguments[_key3];
          }

          return request.apply(_this, ['patch'].concat(args));
      },
      put: function put() {
          for (var _len4 = arguments.length, args = Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
              args[_key4] = arguments[_key4];
          }

          return request.apply(_this, ['put'].concat(args));
      },
      delete: function _delete() {
          for (var _len5 = arguments.length, args = Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
              args[_key5] = arguments[_key5];
          }

          return request.apply(_this, ['delete'].concat(args));
      },
      setFailHandler: function setFailHandler(handler) {
          failHandler = handler;
      }
  };

  return index;

}));