# backbone-http

Sometimes you need to bypass Backbone's built-in models or collection to do a request. E.g. for getting chart data or letting the back-end send the user an email.

If the user then switches between pages before the XHR has been finished, you can get ugly errors because Backbone already has destroyed the view.

This package handles that for you; it will abort a XHR request if the view is destroyed. Under the hood it just uses jQuery for the requests.

## Usage

```js
import http from 'backbone-http';

Marionette.ItemView.extend({
    onRender() {
        const weirdXhr = http.get('api/weird-request', null, { destroyWith: this });

        weirdXhr.done(this.doSomething.bind(this));

        const secondXhr = http.post('api/haha', { foo: 'bar' }, { destroyWith:this });
    }
})
```

Provided methods are: `get`, `post`, `patch`, `put` and `delete`.

The `destroyWith` option is optional. It works for any instance that triggers a `destroy` event (e.g. models, collections, views). When `destroy` is triggered, it will abort the XHR request.

If you want to have a global error handler in your application, do something like this in a bootstrap file:

```js
import http from 'backbone-http';

http.setFailHandler(function(jqXHR, textStatus, errorThrown) {
    console.log('Hey mom I failed the test.');
});
```

You can bypass this by providing `{ skipFailHandler: true }` as an option.

You can add your own parameter to $.ajax by passing a `ajaxOptions` object in the options.

To force the data to be used raw without serialization, add `rawData: true` in the options.
